import views
from django.conf.urls import url, include

urlpatterns = [
	url(r'^$', views.HomeTemplateView.as_view(), name='home-companys'),
	url(r'^conducts/$', views.ListConductsTemplateView.as_view(), name='conducts'),
	url(r'^conducts/add/$', views.AddConductsCreateView.as_view(), name='add-conduct'),
	url(r'^conduct/(?P<pk>[-\d]+)/$', views.ConductDetailView.as_view(), name='detail-conduct'),
]