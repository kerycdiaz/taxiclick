from django.views.generic import TemplateView, CreateView, DetailView
from accounts.models import User

class HomeTemplateView(TemplateView):
	template_name = "companys/home.html"

	def get_context_data(self, **kwargs):
		if not self.request.user.is_authenticated():
			self.template_name = "companys/registro.html"
			
		context = super(HomeTemplateView, self).get_context_data(**kwargs)
		context['conducts'] = User.objects.filter(user_type="CO")
		return context

class ListConductsTemplateView(TemplateView):
	template_name = "companys/lista_conductores.html"

	def get_context_data(self, **kwargs):
		context = super(ListConductsTemplateView, self).get_context_data(**kwargs)
		context['conducts'] = User.objects.filter(user_type="CO")
		return context

class AddConductsCreateView(CreateView):
	model = User
	fields = ["full_name","username","email","password","telephone","vehicle_plate","vehicle_model","vehicle_color","user_type", "avatar"]
	template_name = 'companys/agregar_conductores.html'

class ConductDetailView(DetailView):
	model = User
	template_name = 'companys/single_conductores.html'

