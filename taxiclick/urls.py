"""taxiclick URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from taxiclick.views import HomeView

from rest_framework.authtoken import views

from rest_framework.routers import DefaultRouter
from api import views as api_views
router = DefaultRouter()
router.register(r'users', api_views.UserViewSet)
router.register(r'reservations', api_views.ReservationViewSet)

urlpatterns = [
	url(r'^$', HomeView.as_view(), name='home'),
	url(r'^accounts/', include('allauth.urls')),
	url(r'^accounts/', include('accounts.urls')),
    url(r'^companys/', include('companys.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-token-auth/', views.obtain_auth_token),
]

from django.conf import settings
from django.views.static import serve 
if settings.DEBUG: 
    urlpatterns += [ 
        url(r'^media/(?P<path>.*)$', serve,  {'document_root': settings.MEDIA_ROOT,}), 
    ] 
