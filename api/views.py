from accounts.models import User, Reservation
from accounts.serializer import UserSerializer, ReservationSerializer
from rest_framework.permissions import IsAuthenticated

from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @list_route(permission_classes=[IsAuthenticated])
    def login(self, request):
        user = User.objects.filter(pk=request.user.id)
        serializer = self.get_serializer(user, many=True)
        return Response(serializer.data)

class ReservationViewSet(viewsets.ModelViewSet):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer
