import views
from django.conf.urls import url
from  django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^reservation/$', views.ReservationView.as_view(), name='reservation'),
    url(r'^reservation/(?P<pk>[-\d]+)/$', views.ReservationDetailView.as_view(), name='reservationdetail'),
    url(r'^reservation/(?P<pk>[-\d]+)/success/$', views.ReservationSuccessView.as_view(), name='reservation-success'),
    url(r'^profile/$', login_required(views.UserProfileView.as_view()), name='userprofile'),
    url(r'^(?P<pk>[-\d]+)/update/$', login_required(views.UserUpdateView.as_view()), name='userprofile-update'),
    url(r'^history/$', login_required(views.HistoryReservationView.as_view()), name='history'),
]
