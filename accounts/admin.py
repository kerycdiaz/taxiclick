#coding:utf-8
import models
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin


class UserAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (
            (u'Información adicional', {'fields': ('full_name','telephone','avatar','user_type')}),
    )
admin.site.register(models.User,UserAdmin)
admin.site.register(models.Reservation)