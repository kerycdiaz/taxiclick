from django import forms

class SignupForm(forms.Form):
    full_name = forms.CharField(max_length=100, label='Nombre completo', widget=forms.TextInput(attrs={'placeholder': 'Nombre completo'}))
    telephone = forms.CharField(max_length=20, label='Telefono', widget=forms.TextInput(attrs={'placeholder': 'Telefono'}))

    def signup(self, request, user):
        user.full_name = self.cleaned_data['full_name']
        user.telephone = self.cleaned_data['telephone']
        user.save()