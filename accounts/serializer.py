import models
from rest_framework import serializers
from django.contrib.auth.hashers import make_password

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        """
        fields = ('id', 
            'full_name',
        	'username',
        	'email',  
        	'telephone',
            'identity_card', 
            'vehicle_plate', 
            'vehicle_model', 
            'vehicle_color',
            'user_type',
            'password',
            'avatar',
            'create',
            'is_active',
            'cantRequest',
            'rate',
        )
		"""

    """
    def create(self, validated_data):
          password = validated_data.get('password')
          validated_data['password'] = make_password(password)
          user_type = validated_data.get('user_type')
          if user_type == 'CO':
            validated_data["is_active"] = False
          return models.User.objects.create(**validated_data)
    """
    def update(self, instance, validated_data):
        instance.user_status = validated_data.get('user_status')
        instance.save()
        return instance

class ReservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Reservation