from django.core.serializers import serialize
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models.query import QuerySet
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.utils.safestring import mark_safe
from django.template import Library

register = Library()
from django.forms.models import model_to_dict

from django.core import serializers

@register.filter
def jsonify(obj):
    lista = []
    for x in obj:
    	for b in x.banners.all():
    		dicc = [model_to_dict(x),b.get_banner()]
    		lista.append(dicc)
    if isinstance(lista, QuerySet):
        return mark_safe(serialize('json', lista))
    return mark_safe(json.dumps(lista, cls=DjangoJSONEncoder))

@register.filter
def valtojs(obj):
    try:
        return mark_safe(json.dumps(serializers.serialize("json", [obj]), cls=DjangoJSONEncoder))
    except :
        return mark_safe(json.dumps(serializers.serialize("json", []), cls=DjangoJSONEncoder))