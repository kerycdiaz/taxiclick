from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.template import defaultfilters
from django.contrib.auth.models import AbstractUser

USER_TYPE = (
    ('CL', 'CLIENTE'),
    ('CO', 'CONDUCTOR'),
)

USER_STATUS = (
    ('AC', 'ACTIVO'),
    ('OC', 'OCUPADO'),
    ('IN', 'INACTIVO'),
)

RESERVATION_STATUS = (
    ('RE', 'REALIZADA'),
    ('AC', 'ACEPTADA'),
    ('CA', 'CANCELADA'),
    ('FI', 'FINALIZADA'),
)

class User(AbstractUser):
	full_name = models.CharField(max_length=100)
	telephone = models.CharField(max_length=20, blank=True)
	avatar = models.ImageField(null=True, blank=True)
	user_type = models.CharField(max_length=2, choices=USER_TYPE, default="CL")
	user_status = models.CharField(max_length=2, choices=USER_STATUS, default="IN")
	vehicle_plate = models.CharField(max_length=20,blank=True)
	vehicle_model = models.CharField(max_length=20,blank=True)
	vehicle_color = models.CharField(max_length=20,blank=True)

	def __unicode__(self):
		return self.username

	def get_absolute_url(self):
		return reverse('userprofile-update', kwargs={'pk': self.pk})

	def get_avatar(self):
		if (self.avatar):
			return '%s%s' % (settings.MEDIA_URL,self.avatar)
		else:
			return False

class Reservation(models.Model):
	client = models.ForeignKey(User, related_name="user")
	conduct = models.ForeignKey(User, related_name="conduct", null=True,blank=True)
	since = models.CharField(max_length=100)
	since_address = models.TextField(default='')
	to = models.CharField(max_length=100)
	to_address = models.TextField(default='')
	date = models.DateTimeField(null=True,blank=True)
	people = models.FloatField(null=True,blank=True)
	time = models.FloatField(null=True,blank=True)
	distance = models.FloatField(null=True,blank=True)
	status = models.CharField(max_length=2, choices=RESERVATION_STATUS, default="RE")
	comfirm = models.BooleanField(default=False)

	def __unicode__(self):
		return "Reserva de %s" % self.client.full_name

	def get_absolute_url(self):
		return reverse('reservationdetail', kwargs={'pk': self.id})
