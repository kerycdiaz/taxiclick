# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-19 20:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0010_auto_20160119_2041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='status',
            field=models.CharField(choices=[('RE', 'REALIZADA'), ('AC', 'ACEPTADA'), ('CA', 'CANCELADA'), ('FI', 'FINALIZADA')], default='RE', max_length=2),
        ),
    ]
