import models
from django.core.urlresolvers import reverse
from django.views.generic import TemplateView, UpdateView, DetailView, CreateView

class UserProfileView(TemplateView):
    template_name = "account/user-panel.html"

    def get_context_data(self, **kwargs):
        context = super(UserProfileView, self).get_context_data(**kwargs)
        context['my_reservations'] = models.Reservation.objects.all().filter(status='RE')
        context['ac_reservations'] = models.Reservation.objects.all().filter(status='AC')

        return context

class UserUpdateView(UpdateView):
    model = models.User
    fields = ['full_name','email', 'avatar','telephone','username']
    template_name = "account/user_update_form.html"

class ReservationView(CreateView):
    template_name = "account/reservation.html"
    model = models.Reservation
    fields = ["client","conduct","since","to","since_address","to_address","date","people","time","distance","status"]

    def get_context_data(self, **kwargs):
        context = super(ReservationView, self).get_context_data(**kwargs)
        context['user_id'] = self.request.user.id
        return context

class ReservationDetailView(DetailView):
    template_name = "account/reservation-detail.html"
    model = models.Reservation

class ReservationSuccessView(DetailView):
    template_name = "account/reservation-success.html"
    model = models.Reservation

    def get_context_data(self, **kwargs):
        context = super(ReservationSuccessView, self).get_context_data(**kwargs)
        self.object.comfirm = True
        self.object.save()
        return context

class HistoryReservationView(TemplateView):
    template_name = "account/history.html"

    def get_context_data(self, **kwargs):
        context = super(HistoryReservationView, self).get_context_data(**kwargs)
        context['fi_reservations'] = models.Reservation.objects.all().filter(status='FI')
        context['ca_reservations'] = models.Reservation.objects.all().filter(status='CA')

        return context
