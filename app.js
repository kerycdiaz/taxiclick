var http = require("http");
var server = http.createServer();
var io = require('socket.io')(server);
var port = process.env.PORT || 8080;
var querystring = require('querystring');

server.listen(port, function () {
  console.log('Tu server corre en el puerto %d', port);
});

var admin = false;
var users = {};
var numUsers = 0;
var conductors = [];

if (typeof(Number.prototype.toRad) === "undefined") {
  Number.prototype.toRad = function() {
    return this * Math.PI / 180;
  }
}

function haversine(lat1, lon1, lat2, lon2) {
  var R = 6371;
  var dLat = (lat2 - lat1).toRad();
  var dLong = (lon2 - lon1).toRad();

  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;

  return Math.round(d);
}

function nearbyConductors(user) {
    var lists = [];
    for (x in users){
        var l = users[x];
        if (l.user_type == "CO" && l.user_status == "AC" && l.username != user.username){
          /*console.log(parseFloat(user.latitude))
          console.log(parseFloat(user.longitude))
          console.log("---------------")
          console.log(parseFloat(l.latitude))
          console.log(parseFloat(l.longitude))
          console.log("-----------------------------------")*/
          var km = haversine(parseFloat(user.latitude), parseFloat(user.longitude), parseFloat(l.latitude), parseFloat(l.longitude))
          /*console.log(km)*/
          if (km <= 40){
            var list = [l,km]
            lists.push(list);
          }
        }
    }
    lists.sort(function(a, b) {
        return (a[1] < b[1]) ? -1 : ((a[1] > b[1]) ? 1 : 0);
    });
    return lists
}

function merge_options(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}

function update_status_conduct(conduct){
  console.log(conduct)
  /////////////////////////////
  delete conduct.avatar;
  var url = '/api/users/'+conduct.id+'/';
  var method = "PUT";
  var values=querystring.stringify(conduct);
  var options={
    host: 'localhost',
    port: 8000,
    path: url,
    method: method,
    headers:{
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': values.length,
      'Authorization': 'Token '+conduct.token+''
    }
  }
  var request = http.request(options, function(response){
    response.setEncoding('utf8');
    response.on('data',function(info){
      info = JSON.parse(info);
      console.log(info);
    });
  });

  request.write(values);
  request.end();
  /////////////////////////////
}

io.on('connection', function (socket) {
  var addedUser = false;

  socket.on('add user', function (user) {
    socket.user = user;
    socket.user.sid = socket.id;

    if (socket.user.user_type == 'CO'){ 
      socket.user.user_status = 'AC';
      data = {"conduct": JSON.parse(JSON.stringify(socket.user))};
      update_status_conduct(data.conduct);
    }

    users[socket.user.username] = socket.user;
    ++numUsers;
    addedUser = true;
    socket.emit('login', {
      numUsers: numUsers
    });

    /*socket.broadcast.emit('user joined', {
      username: socket.user.username,
      numUsers: numUsers
    });
    if (socket.user.username == "admin"){
      admin = socket.id;
      socket.emit('users', {
        users: users,
        numUsers: numUsers
      });
    }
    if (admin){
      socket.broadcast.to(admin).emit('users', {
        users: users,
        numUsers: numUsers
      });
    }*/
  });

  socket.on('sendLatLng',function(data){
      socket.user.latitude = data.conduct.latitude;
      socket.user.longitude = data.conduct.longitude;

      //Mostrarle donde estoy al cliente
      if (typeof data.client != "undefined" && typeof users[data.client.username] != "undefined" && data.share)
        socket.broadcast.to(users[data.client.username].sid).emit('returnLatLng', data);
  });

  socket.on('searchConducts',function(data){
    data.user = merge_options(data.user, socket.user) //devolver solo lo necesario despues  
    conductors = nearbyConductors(data.user);
    socket.emit('listConducts', {"conductors": conductors});
  });

  socket.on('requestTaxi',function(data){
    if (typeof users[data.conduct.username] != "undefined")
      socket.broadcast.to(users[data.conduct.username].sid).emit('responseTaxi', data);
    else
      console.log("conductor no disponible intente mas tarde")
  });

  socket.on('requestTaxiAccept',function(data){   
      if (typeof users[data.conduct.username] != "undefined" && data.status == "RE"){
        data.status = 'AC';

        socket.broadcast.to(users[data.client.username].sid).emit('requestTaxiAccept', data);
        socket.emit('requestTaxiAccept', data);

        /////////////////////////////
        users[data.conduct.username].user_status = 'OC';
        update_status_conduct(users[data.conduct.username])
        /////////////////////////////

        var url = '/api/reservations/'+data.id+'/';
        var method = "PUT";
        data.client = data.client.id;
        data.conduct = data.conduct.id;
        
        /////////////////////////////
        var values=querystring.stringify(data);
        var options={
          host: 'localhost',
          port: 8000,
          path: url,
          method: method,
          headers:{
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': values.length,
            'Authorization': 'Token '+socket.user.token+''
          }
        }
        var request = http.request(options, function(response){
          response.setEncoding('utf8');
          response.on('data',function(info){
            info = JSON.parse(info)
          });
        });

        request.write(values);
        request.end();
        /////////////////////////////
      }else{
        console.log("cliente no disponible intentar mas tarde")
      }
  });

  socket.on('requestTaxiDeny',function(data){ 
      if (typeof users[data.client.username] != "undefined" && data.status == 'RE'){
        data.status = 'CA';

        socket.broadcast.to(users[data.client.username].sid).emit('responseTaxiDeny', data);

        var url = '/api/reservations/'+data.id+'/';
        var method = "PUT";
        data.client = data.client.id;
        data.conduct = data.conduct.id;
        
        var values=querystring.stringify(data);
        var options={
          host: 'localhost',
          port: 8000,
          path: url,
          method: method,
          headers:{
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': values.length,
            'Authorization': 'Token '+socket.user.token+''
          }
        }
        var request = http.request(options, function(response){
          response.setEncoding('utf8');
          response.on('data',function(info){
            info = JSON.parse(info)
            console.log(info)
          });
        });

        request.write(values);
        request.end();
      }else{
        console.log("cliente no diponible intentar mas tarde")
      }

      /*if (typeof data.user.accept != "undefined" && !data.user.accept){
        if (typeof data.nearby == "undefined"){
          data.nearby = 0;
        }
        data.nearby += 1
        if (conductors[data.nearby]){
          socket.broadcast.to(data.conduct.sid).emit('responseTaxi', data);

          data.user.accept = undefined;
          console.log("Proximo Conductor mas cercano " + conductors[data.nearby][0].username)
          socket.broadcast.to(conductors[data.nearby][0].sid).emit('responseTaxi', data);
        }else{
          socket.emit('responseTaxi', data);
          socket.broadcast.to(data.conduct.sid).emit('responseTaxi', data);

          var url = '/requestTaxi/'+data.id+'/';
          var method = "PUT";
          var values=querystring.stringify({"status":"CA",'user':data.user.id,'conduct':data.conduct.id});
          var options={
            host: '158.69.199.106',
            port: 8001,
            path: url,
            method: method,
            headers:{
              'Content-Type': 'application/x-www-form-urlencoded',
              'Content-Length': values.length,
              'Authorization': 'Token '+socket.user.token+''
            }
          }
          var request = http.request(options, function(response){
            response.setEncoding('utf8');
            response.on('data',function(info){
              console.log("1: ")
              console.log(info)
              data.conduct.status = 'YIN';
              users[data.conduct.id] = data.conduct;
              if (admin){
                socket.broadcast.to(admin).emit('users', {
                  users: users,
                  numUsers: numUsers
                });
                delete users[data.conduct.id]
              }

            });
          });

          request.write(values);
          request.end();
        }
      }else{
        if (typeof data.user.cancel == "undefined" && typeof data.conduct.cancel == "undefined"){
          data.nearby += 1
          if (conductors[data.nearby]){
            console.log("Proximo Conductor mas cercano " + conductors[data.nearby][0].username)
            socket.broadcast.to(conductors[data.nearby][0].sid).emit('responseTaxi', data);
          }else{
            data.conduct.accept = false;
            socket.broadcast.to(data.user.sid).emit('responseTaxi', data);
          }
        }else{
          socket.broadcast.to(data.user.sid).emit('responseTaxi', data);
          socket.broadcast.to(data.conduct.sid).emit('responseTaxi', data);

          var url = '/requestTaxi/'+data.id+'/';
          var method = "PUT";
          var values=querystring.stringify({"status":"CA",'user':data.user.id,'conduct':data.conduct.id});
          var options={
            host: '158.69.199.106',
            port: 8001,
            path: url,
            method: method,
            headers:{
              'Content-Type': 'application/x-www-form-urlencoded',
              'Content-Length': values.length,
              'Authorization': 'Token '+socket.user.token+''
            }
          }
          var request = http.request(options, function(response){
            response.setEncoding('utf8');
            response.on('data',function(info){
              console.log(info)
              data.conduct.status = 'YIN';
              users[data.conduct.id] = data.conduct;
              if (admin){
                socket.broadcast.to(admin).emit('users', {
                  users: users,
                  numUsers: numUsers
                });
                delete users[data.conduct.id]
              }

            });
          });

          request.write(values);
          request.end();
        }
      }*/
  });

  socket.on('requestTaxiFinish',function(data){ 
    data.status = 'FI';

    socket.broadcast.to(users[data.client.username].sid).emit('responseTaxiFinish', data);
   
    /////////////////////////////
    users[data.conduct.username].user_status = 'IN';
    update_status_conduct(users[data.conduct.username])
    /////////////////////////////

    /////////////////////////////
    var url = '/api/reservations/'+data.id+'/';
    var method = "PUT";
    data.client = data.client.id;
    data.conduct = data.conduct.id;
        
    var values=querystring.stringify(data);
    var options={
      host: 'localhost',
      port: 8000,
      path: url,
      method: method,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': values.length,
        'Authorization': 'Token '+socket.user.token+''
      }
    }
    var request = http.request(options, function(response){
      response.setEncoding('utf8');
      response.on('data',function(info){
        info = JSON.parse(info)
        console.log(info)
      });
    });

    request.write(values);
    request.end();
    /////////////////////////////
  });

  socket.on('requestTaxiCancel',function(data){ 
    data.status = 'CA';
    socket.user.token = data.conduct.token;

    /////////////////////////////
    users[data.conduct.username].user_status = 'IN';
    update_status_conduct(users[data.conduct.username])
    /////////////////////////////
    
    /////////////////////////////
    var url = '/api/reservations/'+data.id+'/';
    var method = "PUT";
    data.client = data.client.id;
    data.conduct = data.conduct.id;
        
    var values=querystring.stringify(data);
    var options={
      host: 'localhost',
      port: 8000,
      path: url,
      method: method,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': values.length,
        'Authorization': 'Token '+socket.user.token+''
      }
    }
    var request = http.request(options, function(response){
      response.setEncoding('utf8');
      response.on('data',function(info){
        info = JSON.parse(info)
        console.log(info)
      });
    });

    request.write(values);
    request.end();
    /////////////////////////////
  });

  socket.on('requestPista',function(data){
    socket.broadcast.to(data.conduct.sid).emit('responsePista', data);
  });

  socket.on('requestMessage',function(data){
    if (socket.user.username == data.user.id)
      socket.broadcast.to(data.conduct.sid).emit('responseMessage', data);  
    else if (socket.user.username == data.conduct.id)
      socket.broadcast.to(data.user.sid).emit('responseMessage', data);
  });

  socket.on('requestHelp',function(data){
    var url = '/AlertsWrite/';
        var method = "POST";
        var info = {};
        info.requestTaxi = data.requestTaxi;
        var values=querystring.stringify(info);
        var options={
          host: '158.69.199.106',
          port: 8001,
          path: url,
          method: method,
          headers:{
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': values.length,
            'Authorization': 'Token '+socket.user.token+''
          }
        }
        var request = http.request(options, function(response){
          response.setEncoding('utf8');
          response.on('data',function(info){
            info = JSON.parse(info)
            if (admin){
              socket.broadcast.to(admin).emit('responseHelp', {
                data: info
              });
            }
          });
        });

        request.write(values);
        request.end();
  });

  socket.on('requestTaxiFuture',function(data){
    var url = '/requestTaxiFuture/';
    var method = "POST";
    var info = {};
    info.fromLat = data.user.from.latitude;
    info.fromLng = data.user.from.longitude;
    info.fromFormatted_address = data.user.from.formatted_address;
    info.toFormatted_address = data.user.to.formatted_address;
    info.datetime = data.datetime;
    info.user = data.user.id;
    var values=querystring.stringify(info);
    var options={
      host: '158.69.199.106',
      port: 8001,
      path: url,
      method: method,
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': values.length,
        'Authorization': 'Token '+socket.user.token+''
      }
    }
    var request = http.request(options, function(response){
      response.setEncoding('utf8');
      response.on('data',function(info2){
        info2 = JSON.parse(info2) 
        if (admin){
          socket.broadcast.to(admin).emit('responseTaxiFuture', {
            data: info2
          });
        }
      });
    });

    request.write(values);
    request.end();
  });

  socket.on('disconnect', function () {
    if (addedUser) {

      /////////////////////////////
      if (socket.user.user_type == 'CO'){
        socket.user.user_status = 'IN';
        data = {"conduct": JSON.parse(JSON.stringify(socket.user))};
        update_status_conduct(data.conduct)
      }
      /////////////////////////////

      delete users[socket.user.username];
      --numUsers;

      socket.broadcast.emit('user left', {
        username: socket.user.username,
        numUsers: numUsers
      });
    }
  });
});