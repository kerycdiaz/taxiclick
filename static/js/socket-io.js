
var socket = io('http://192.99.54.226:3000');
var reservations = {}
socket.emit('add user', user);

//Evento cuando me he conectado correctamente
socket.on('login', function (data) {
	console.log("connected: " + user.username)
	console.log(data)
});

//Evento cuando me he desconectado correctamente
socket.on('user left', function (data) {
  if(typeof reservations[data.username] != "undefined"){
  	socket.emit('requestTaxiCancel', reservations[data.username]);
  	alert("Hola el conductor " + data.username + " ha cancelado la carrera")
  	window.location = "/accounts/history/";
  }
});

//Evento cuando un conductor no acepto la solicitud de taxi
socket.on('responseTaxiDeny', function (data) {
	alert("Hola el conductor " + data.conduct.username + " ha denegado tu peticion")
  	window.location = "/accounts/history/";
});

//Evento cuando un conductor no acepto la solicitud de taxi
socket.on('requestTaxiAccept', function (data) {
	alert("Hola el conductor " + data.conduct.username + " ha aceptado tu peticion")
  	window.location = "/accounts/profile/";
});

//Evento para recibir la ubicacion del conductor
socket.on('returnLatLng', function (data) {
  	reservations[data.conduct.username] = data;
  	reservation = reservations[data.conduct.username];
  	console.log(reservation)

  	var initial = new google.maps.LatLng(reservation.client.latitude,reservation.client.longitude);
	var destination = new google.maps.LatLng(reservation.conduct.latitude,reservation.conduct.longitude);
	var directionsService = new google.maps.DirectionsService();
	var request = {
		origin:initial,
		destination:destination,
		travelMode: google.maps.TravelMode.DRIVING
	};
	
	directionsService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			duration = result.routes[0].legs[0].duration.text;
			distance = result.routes[0].legs[0].distance.text; 
			var time = parseFloat(result.routes[0].legs[0].duration.text.split(" ")[0])
			var dist = parseFloat(result.routes[0].legs[0].distance.text.split(" ")[0])   
			$('#reservation-'+reservation.id+'').find(".conduct_time").html(time);
  			$('#reservation-'+reservation.id+'').find(".conduct_distance").html(dist);         
		}
	});		
});

//Evento cuando un conductor no acepto la solicitud de taxi
socket.on('responseTaxiFinish', function (data) {
	alert("Hola el conductor " + data.conduct.username + " ha llegado a tu ubicacion")
  	window.location = "/accounts/history/";
});

