navigator.geolocation.getCurrentPosition(function(position){
	map.setCenter(new google.maps.LatLng(position.coords.latitude,position.coords.longitude));
	map.setZoom(14);

	$('#latitude').text(position.coords.latitude);
	$('#longitude').text(position.coords.longitude);
	newMarker.setPosition(new google.maps.LatLng(position.coords.latitude,position.coords.longitude));

	$('#latitude-for').text(position.coords.latitude + 0.010060);
	$('#longitude-for').text(position.coords.longitude);
	newMarker2.setPosition(new google.maps.LatLng(position.coords.latitude + 0.010060,position.coords.longitude));

	socket.emit('searchConducts', { 'user': {"latitude": position.coords.latitude, 'longitude': position.coords.longitude } });
});

if (user.user_type == 'CL'){
	socket.emit('searchConducts', { 'user': {"latitude": $('#latitude').text(), 'longitude': $('#longitude').text() } });
		
	//Obtener listado de conductores cercanos
	socket.on('listConducts', function (data) {
		$('.results.listConducts').html("");

		if (data.conductors.length != 0){
			for (c in data.conductors){
				conduct = data.conductors[c][0];
				kilometer = data.conductors[c][1];
				console.log(conduct)
				//avatar = "http://192.99.54.226/media" + conduct.avatar.split(".")[1] + "." + conduct.avatar.split(".")[2];
				distance = 0;
				duration = 0;

				var initial = new google.maps.LatLng(parseFloat($('#latitude').text()),parseFloat($('#longitude').text()));
				var destination = new google.maps.LatLng(conduct.latitude,conduct.longitude);
				var directionsService = new google.maps.DirectionsService();
				var request = {
					origin:initial,
					destination:destination,
					travelMode: google.maps.TravelMode.DRIVING
				};
				directionsService.route(request, function(result, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						duration = result.routes[0].legs[0].duration.text;
						distance = result.routes[0].legs[0].distance.text; 
						var time = parseFloat(result.routes[0].legs[0].duration.text.split(" ")[0])
						var dist = parseFloat(result.routes[0].legs[0].distance.text.split(" ")[0])            
						var since_address = result.routes[0].legs[0].start_address;
						var to_address = result.routes[0].legs[0].end_address;
					}else{
						distance = String(kilometer) + 'km';
						duration = 'Dificil de calcular';
						var time = null;
						var dist = String(kilometer);
					}

					var since = String(initial.lat()) + ',' + String(initial.lng())
					var to = String(destination.lat()) + ',' + String(destination.lng())
					$('.results.listConducts').append(''+
						'<article class="result">'+
						'<div class="one-fourth heightfix"><img width="100%" src="'+conduct.avatar+'" alt="" /></div>'+
						'<div class="one-half heightfix">'+
						'<h3>'+conduct.full_name+'<a href="javascript:void(0)" class="trigger color" title="Read more">?</a></h3>'+
						'<ul>'+
						'<li>'+
						'<i class="fa fa-clock-o fa-3x color-ico"></i>'+
						'<p>TIEMPO ESTIMADO <strong> <br />'+duration+'</strong></p>'+
						'</li>'+
						'<li>'+
						'<i class="fa fa-road fa-3x color-ico"></i>'+
						'<p>DISTANCIA <strong> <br />'+distance+'</strong></p>'+
						'</li>'+
						'</ul>'+
						'</div>'+
						'<div class="one-fourth heightfix">'+
						'<div>'+
						'<form action="/accounts/reservation/" method="post">'+
						'<input type="hidden" name="csrfmiddlewaretoken" value="'+csrf_token+'">'+
						'<input type="hidden" name="client" value="'+user_id+'">'+
						'<input type="hidden" name="conduct" value="'+conduct.id+'">'+
						'<input type="hidden" name="since" value="'+since+'">'+
						'<input type="hidden" name="since_address" value="'+since_address+'">'+
						'<input type="hidden" name="to" value="'+to+'">'+
						'<input type="hidden" name="to_address" value="'+to_address+'">'+
						'<input type="hidden" name="time" value="'+time+'">'+
						'<input type="hidden" name="distance" value="'+dist+'">'+
						'<input type="hidden" name="status" value="RE">'+
						'<button class="btn grey large" type="submit">Seleccionar</button>'+
						'</form>'+
						'</div>'+
						'</div>'+
						'</article>'+
						'')
					$('#search_conducts').html("Buscar conductores");
					$('.message_loading').hide();
					$('#listConducts').show();
				});		
			}
		}else{
			$('#search_conducts').html("Buscar conductores");
			$('.message_loading').show();
			$('#listConducts').hide();
		}
	});
}

$('#search_conducts').click(function(){
	$('#search_conducts').html("Espera...");
	socket.emit('searchConducts', { 'user': {"latitude": $('#latitude').text(), 'longitude': $('#longitude').text() } });
})